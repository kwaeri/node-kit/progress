# [![Patreon](https://img.shields.io/badge/Patreon-Funding-inactive?style=for-the-badge&logo=patreon&color=FF424D)](https://patreon.com/kirvedx) kwaeri-cli-progress [![PayPal](https://img.shields.io/badge/PayPal-Donations-inactive?style=for-the-badge&logo=paypal&color=253B80)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)

A Massively Modified Open Source Project by kirvedx

[![GPG/Keybase](https://img.shields.io/badge/GPG-1B842CB5%20Rik-inactive?style=for-the-badge&label=GnuPG2%2FKeybase&logo=gnu+privacy+guard&color=0093dd)](https://keybase.io/rik)
[![Google](https://img.shields.io/badge/Google%20Developers-kirvedx-inactive?style=for-the-badge&logo=google+tag+manager&color=414141)](https://developers.google.com/profile/u/117028112450485835638)
[![GitLab](https://img.shields.io/badge/GitLab-kirvedx-inactive?style=for-the-badge&logo=gitlab&color=fca121)](https://github.com/kirvedx)
[![GitHub](https://img.shields.io/badge/GitHub-kirvedx-inactive?style=for-the-badge&logo=github&color=181717)](https://github.com/kirvedx)
[![npm](https://img.shields.io/badge/NPM-Rik-inactive?style=for-the-badge&logo=npm&color=CB3837)](https://npmjs.com/~rik)

The @kwaeri/progress component for the @kwaeri/user-executable framework

[![pipeline status](https://gitlab.com/kwaeri/cli/progress/badges/main/pipeline.svg)](https://gitlab.com/kwaeri/cli/progress/commits/main)  [![coverage report](https://gitlab.com/kwaeri/cli/progress/badges/main/coverage.svg)](https://kwaeri.gitlab.io/cli/progress/coverage/)  [![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/1879/badge)](https://bestpractices.coreinfrastructure.org/projects/1879)

## TOC
* [The Implementation](#the-implementation)
* [Getting Started](#getting-started)
  * [Installation](#installation)
  * [Usage](#usage)
* [How to Contribute Code](#how-to-contribute-code)
* [Other Ways to Contribute](#other-ways-to-contribute)
  * [Bug Reports](#bug-reports)
  * [Vulnerability Reports](#vulnerability-reports)
    * [Confidential Issues](#confidential-issues)
  * [Donations](#donations)

## The Implementation

The progress component module provides methods for implementing a progress bar in a terminal/command prompt that displays the progress of - and notifications for - processes invoked by end-users of @kwaeri/user-executable [kue].

@kwaeri/progress was originally included in the entry point to the platform under the directory [from root] `node-kit/external/`. In an attempt to standardize implementation - by separating concerns - as well as to ease maintainence of each component of the platform, the progress was migrated to its own component module.

## Getting Started

**NOTE**

@kwaeri/progress is not ready for production. We've published this module for testing and development purposes. You're free to try out anything that may already be available, but please be aware that there is likely to be many aspects of the platform which are not working and/or are completely broken. As we near completion of the new platform, we'll update documentation and provide complete examples and tutorials for getting started.

### Installation

[@kwaeri/node-kit](https://www.npmjs.com/package/@kwaeri/node-kit) wraps the various components under the kwaeri scope, and provides a single entry point to the node-kit platform for easing the process of building a kwaeri application.

[@kwaeri/cli](https://www.npmjs.com/package/@kwaeri/cli) wraps the various CLI components under the @kwaeri scope, and provides a single entry point to the user executable framework.

If you wish to use @kwaeri/progress - perform the following steps:

Install @kwaeri/progress:

```bash
npm install --save @kwaeri/progress
```

### Usage

The progress component is leveraged for showing a progress bar and its notifications to end-users of [kue](https://www.gitlab.com/kwaeri/cli/user-executable), through the Steward, explicitly in its handling of commands passed to it by the end user requesting automation of sorts - which is pretty much the core purpose of the CLI.

To leverage the progress component, you'll first need to include it:

```javascript
// INCLUDES
import { Progress } from '@kwaeri/progress';

// ...
```

Next, you'll want to instantiate it. You can choose a [type?](#constructor), whether to show the [spinner](#constructor), which spin [animation](#constructor) to use,  and/or [percentage](#constructor), the preferred progress bar length, or accept the defaults.


###### Constructor
```typescript
/**
 * @param { string } type The type of the progress bar. Defaults to "bar"
 * @param { boolean } spinner Flags wether to display the spinner with the progress bar.
 * @param { string } spinAnim The spin animatin to use should spinner be true - defaults to "dots".
 * @param { boolean } percentage Flags whether to display the percentage with the progress bar
 * @param { number } barLength The number of columns to use for displaying the progress bar
 */
constructor( { type?: string, spinner?: boolean, spinAnim?: string, percentage?: boolean, barLength?: number } = { type: "bar", spinner: true, spinAnim: "dots", percentage: false, barLength: 25 } ): void;
```

* The default bar length is 25 characters, unless the number of columns does not allow for this - in which case the bar length will be the number of columns available, minus any characters required for rendering the progress bar decorations.
* The spin animation is the well-known "dots" animation by default. There are many animations to choose from - view `./src/assets/spinners.mts` for a reference.
  * The character frames for many of the available spinners were "nabbed" from [this stack overflow](https://stackoverflow.com/a/12305221/2041005) referencing this [json](https://raw.githubusercontent.com/sindresorhus/cli-spinners/master/spinners.json) file, ultimately borrowed from [cli-spinners](https://www.npmjs.com/package/cli-spinners).
  * Originally, it was a hard-coded "lines" spinner on an interval of 100ms, and then it was a hardcoded "dots" spinner at an interval of 100ms. The decision to provide the lot of precomposed spinner sequences was based upon the fact that someone else put together the JSON object for them - complete with reasonable interval values; I was not going to put in that much effort.
  * I did reorder some of the spinner's frames where they seemed completely out of sequence - for example, the moon phase sequence.

```javascript
// DEFINES

/* The argument object is optional, as are any properties */
let progress = new Progress( { type: "bar", spinner: true, spinAnim: "dots", percentage: false, barLength: 25 } );

//...
```

Finally, you'll want to [init](#init)ialize the progress bar. You can set the [current](#init) and [total](#init) level of progress - though defaults of `0` and `100` exist, respectively:


###### Init
```typescript
/**
 * @param { number } current The current level of progress to set
 * @param { number } total The total level of progress that can be set
 *
 * @returns { void }
 */
public init( current: number = 0, total: number = 100 ): void;
```

Using [init](#init) typically involves simply calling it:

```javascript
// Initialize - and render - the progress bar.
progress.init();

//...
```

Initializing the progress bar renders it, so at this point you'll want to make use of Progress facilities to handle all output to the console. Several methods exist to aid in this:

#### Update

The `update()` method allows you to pass in a [progress level](#current):

```typescript
/**
 * @param { number } current The current level of progress to display
 *
 * @returns { void }
 */
public update( current?: number ): void;
```

`update()` is typically called to increase progress:

```javascript
// Update progress to 10%
progress.update( 10 );

//...
```

#### Notify

The `notify()` method allows you to pass in a [notice](#notice) to be displayed beside the progress bar:

```typescript
/**
 * @param { string } notice The string notification to be written to stdout
 *
 * @returns { void }
 */
public notify( notice: string = "" ): void;
```



```javascript
// Alert the user to the current action
progress.notify( `Downloading file: ${_someFiles[i]}...` );

//...
```

You can invoke the method with out any arguments in order to clear the notification as well.

#### UpdateAndNotify

The `updateAndNotify()` method allows you to update the level of progress and the notification being displayed simultaneously:

```typescript
/**
 * @param { number } current The current level of proegress to display
 * @param { string } notice The notice to display beside the progress bar. May be left blank to clear
 *
 * @returns { void }
 */
public updateAndNotify( current: number, notice?: = "" ): void;
```

The `updateAndNotify()` method's second argument by default is an empty string, allowing you leave it out:

```javascript
// Completed!
progress.updateAndNotify( 100, "Download complete." );

//...
```

`updateAndNotify()` simply sets the internal `notification` property value and subsequently calls `update()` in order to accomplish it's function.

#### Log

The `log()` method allows the caller to essentially perform a `console.log()` when using the progress component - with out breaking the progress bar display.

```typescript
/**
 * @param { string } message The string message to log to the console window
 * @param { number } type The type of the message being logged (INFO, ERROR< IMPORTANT, CRITICAL)
 *
 * @returns { Progress } Returns `this` to allow chaining
 */
public log( message: string, type: number = 0 ): this;
```

* `log()` accepts a type value, which determines what flag is displayed with the  log item. 0 = INFO, 1 = ERROR, 2 = IMPORTANT, 3 = CRITICAL. The default is 0 (INFO).
* `log()` returns `this` to allow chaining.

```javascript
// Log completed file download:
progress.log( `File ${_someFiles[i]} downloaded successfully.` ).updateAndNotify( Math.round( ( ( i * 100 ) / _someFiles.length ) ), `Preparing to download ${_someFiles[i+1]}` );

//...
```

The `log()` method is the tool that brings it all together and enables leveraging the component module for a variety of tasks.


## How to Contribute Code

Our Open Source projects are always open to contribution. If you'd like to cocntribute, all we ask is that you follow the guidelines for contributions, which can be found at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Contribute-Code)

There you'll find topics such as the guidelines for contributions; step-by-step walk-throughs for getting set up, [Coding Standards](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Coding-Standards), [CSS Naming Conventions](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/CSS-Naming-Conventions), and more.

The project also leverages Keybase for communication and alerts - outside of standard email. To join our keybase chat, run the following from terminal (assuming you have [keybase](https://www.keybase.io) installed and running):

```bash
keybase team request-access kwaeri
```

Alternatively, you could search for the team in the GUI application and request access from there.

## Other Ways to Contribute

There are other ways to contribute to the project other than with code. Consider [testing](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Test-Code) the software, or in case you've found an [Bug](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports) - please report it. You can also support the project monetarly through [donations](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Donations) via PayPal.

Regardless of how you'd like to contribute, you can also find in-depth information for how to do so at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute)

### Bug Reports

To submit bug reports, request enhancements, and/or new features - please make use of the **issues** system baked-in to our source control project space at [Gitlab](https://gitlab.com/groups/kwaeri/node-kit/-/issues)

You may optionally start an issue, track, and manage it via email by sending an email to our project's [Service Desk](mailto:contact-project+kwaeri-cli-progress-20989565-issue-@incoming.gitlab.com).

For more in-depth documentation on the process of submitting bug reports, please visit the [Massively Modified Wiki on Bug Reports](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports)

### Vulnerability Reports

Our Vulnerability Reporting process is very similar to Gitlab's. In fact, you could say its a *fork*.

To submit vulnerability reports, please email our [Security Group](mailto:security@mmod.co). We will try to acknowledge receipt of said vulnerability by the next business day, and to also provide regular updates about our progress. If you are curious about the status of your report feel free to email us again. If you wish to encrypt your disclosure email, like with gitlab - please email us to ask for our GPG Key.

Please refrain from requesting compensation for reporting vulnerabilities. We will publicly acknowledge your responsible disclosure, if you request us to do so. We will also try to make the confidential issue public after the vulnerability is announced.

You are not allowed, and will not be able, to search for vulnerabilities on Gitlab.com. As our software is open source, you may download a copy of the source and test against that.

#### Confidential Issues

When a vulnerability is discovered, we create a [confidential issue] to track it internally. Security patches will be pushed to private branches and eventually merged into a `security` branch. Security issues that are not vulnerabilites can be seen on our [public issue tracker](https://gitlab.com/groups/kwaeri/node-kit/-/issues?scope=all&utf8=✓&state=opened&label_name[]=Security).

For more in-depth information regarding vulnerability reports, confidentiality, and our practices; Please visit the [Massively Modified Wiki on Vulnerability](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Vulnerability-Reports)

### Donations

If you cannot contribute time or energy to neither the code base, documentation, nor community support; please consider making a monetary contribution which is extremely useful for maintaining the Massively Modified network and all the goodies offered free to the public.

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)
