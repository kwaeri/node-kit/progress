# License

This work provides an option for either the Apache-2.0 WITH LLVM-exception
license or the MIT license.

As a rule of thumb:

| LICENSE | GIST |
| :---  | :--- |
| MIT   | Typically compatible with all other license types. Short, easy to read, anyone can use, copy, modify, distribute or sell the code so long as they preserve the MIT license and copyright notice. There are no patent right grants. |
| Apache-2.0 | Typically compatible with most other licenses. Not compatible with L/GPL2.0. Longer, more difficult to read (but your lawyer will thank you!). Anyone can use, copy, modify, distribute or sell the code so long as they preserve the Apache-2.0 license and copyright notice. A patent right grant is provided for products that use the original or derived work(s) and protects the author and contributors from infringement claims based on contributions by denying the right to patent the work directly. It also protects the author and contributors of others gaining claim to patent rights of others works that similarly leverage the original or derived works. |
| Apache-2.0 With LLVM Exception | L/GPL2.0 Compatible version of the Apache 2.0 license. |

In short:

* Most people choose the MIT because its simple and easy to use; "Do what ever you want, just don't sue me".
* The Apache License is used for more refined certainty and to be more permissive for proprietary use. It's even more permissive - and grants a larger scope of protection - than the MIT license.
* The LLVM Exception allows the Apache-2.0 work to be combined with a L/GPL2.0 work, when the exception is applied to the work (you can't add the exception to an otherwise pure Apache-2.0 work - you can only define your work to possess that exception to the Apache-2.0 license from the get-go).

To see full SPDX compliant license and copyright information please see the
`DEP5/copyright` file, or reference any respective license specified in a file
header comment with `SPDX-LicenseIdentifier: XXX` from the `LICENSES` directory.

## Copying

Complete copyright information can be found in the `copyright` file
located under the `DEP5` directory within the root of this repository.

To review the list of contributors, see the `AUTHORS.md` file in the
root of this repository.

## Apache-2.0 WITH LLVM-exception

Purely for convenience, the full Apache-2.0 WITH LLVM-exception text and
relavent third party notice(s) is/are provided:

                            Apache License
                       Version 2.0, January 2004
                    http://www.apache.org/licenses/

   TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION

   1. Definitions.

      "License" shall mean the terms and conditions for use, reproduction,<br />
      and distribution as defined by Sections 1 through 9 of this document.

      "Licensor" shall mean the copyright owner or entity authorized by<br />
      the copyright owner that is granting the License.

      "Legal Entity" shall mean the union of the acting entity and all<br />
      other entities that control, are controlled by, or are under common<br />
      control with that entity. For the purposes of this definition,<br />
      "control" means (i) the power, direct or indirect, to cause the<br />
      direction or management of such entity, whether by contract or<br />
      otherwise, or (ii) ownership of fifty percent (50%) or more of the<br />
      outstanding shares, or (iii) beneficial ownership of such entity.

      "You" (or "Your") shall mean an individual or Legal Entity<br />
      exercising permissions granted by this License.

      "Source" form shall mean the preferred form for making modifications,<br />
      including but not limited to software source code, documentation<br />
      source, and configuration files.

      "Object" form shall mean any form resulting from mechanical<br />
      transformation or translation of a Source form, including but<br />
      not limited to compiled object code, generated documentation,<br />
      and conversions to other media types.

      "Work" shall mean the work of authorship, whether in Source or<br />
      Object form, made available under the License, as indicated by a<br />
      copyright notice that is included in or attached to the work<br />
      (an example is provided in the Appendix below).

      "Derivative Works" shall mean any work, whether in Source or Object<br />
      form, that is based on (or derived from) the Work and for which the<br />
      editorial revisions, annotations, elaborations, or other modifications<br />
      represent, as a whole, an original work of authorship. For the purposes<br />
      of this License, Derivative Works shall not include works that remain<br />
      separable from, or merely link (or bind by name) to the interfaces of,<br />
      the Work and Derivative Works thereof.

      "Contribution" shall mean any work of authorship, including<br />
      the original version of the Work and any modifications or additions<br />
      to that Work or Derivative Works thereof, that is intentionally<br />
      submitted to Licensor for inclusion in the Work by the copyright owner<br />
      or by an individual or Legal Entity authorized to submit on behalf of<br />
      the copyright owner. For the purposes of this definition, "submitted"<br />
      means any form of electronic, verbal, or written communication sent<br />
      to the Licensor or its representatives, including but not limited to<br />
      communication on electronic mailing lists, source code control systems,<br />
      and issue tracking systems that are managed by, or on behalf of, the<br />
      Licensor for the purpose of discussing and improving the Work, but<br />
      excluding communication that is conspicuously marked or otherwise<br />
      designated in writing by the copyright owner as "Not a Contribution."

      "Contributor" shall mean Licensor and any individual or Legal Entity<br />
      on behalf of whom a Contribution has been received by Licensor and<br />
      subsequently incorporated within the Work.

   2. Grant of Copyright License. Subject to the terms and conditions of<br />
      this License, each Contributor hereby grants to You a perpetual,<br />
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable<br />
      copyright license to reproduce, prepare Derivative Works of,<br />
      publicly display, publicly perform, sublicense, and distribute the<br />
      Work and such Derivative Works in Source or Object form.

   3. Grant of Patent License. Subject to the terms and conditions of<br />
      this License, each Contributor hereby grants to You a perpetual,<br />
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable<br />
      (except as stated in this section) patent license to make, have made,<br />
      use, offer to sell, sell, import, and otherwise transfer the Work,<br />
      where such license applies only to those patent claims licensable<br />
      by such Contributor that are necessarily infringed by their<br />
      Contribution(s) alone or by combination of their Contribution(s)<br />
      with the Work to which such Contribution(s) was submitted. If You<br />
      institute patent litigation against any entity (including a<br />
      cross-claim or counterclaim in a lawsuit) alleging that the Work<br />
      or a Contribution incorporated within the Work constitutes direct<br />
      or contributory patent infringement, then any patent licenses<br />
      granted to You under this License for that Work shall terminate<br />
      as of the date such litigation is filed.

   4. Redistribution. You may reproduce and distribute copies of the<br />
      Work or Derivative Works thereof in any medium, with or without<br />
      modifications, and in Source or Object form, provided that You<br />
      meet the following conditions:

      (a) You must give any other recipients of the Work or<br />
          Derivative Works a copy of this License; and

      (b) You must cause any modified files to carry prominent notices<br />
          stating that You changed the files; and

      (c) You must retain, in the Source form of any Derivative Works<br />
          that You distribute, all copyright, patent, trademark, and<br />
          attribution notices from the Source form of the Work,<br />
          excluding those notices that do not pertain to any part of<br />
          the Derivative Works; and

      (d) If the Work includes a "NOTICE" text file as part of its<br />
          distribution, then any Derivative Works that You distribute must<br />
          include a readable copy of the attribution notices contained<br />
          within such NOTICE file, excluding those notices that do not<br />
          pertain to any part of the Derivative Works, in at least one<br />
          of the following places: within a NOTICE text file distributed<br />
          as part of the Derivative Works; within the Source form or<br />
          documentation, if provided along with the Derivative Works; or,<br />
          within a display generated by the Derivative Works, if and<br />
          wherever such third-party notices normally appear. The contents<br />
          of the NOTICE file are for informational purposes only and<br />
          do not modify the License. You may add Your own attribution<br />
          notices within Derivative Works that You distribute, alongside<br />
          or as an addendum to the NOTICE text from the Work, provided<br />
          that such additional attribution notices cannot be construed<br />
          as modifying the License.

      You may add Your own copyright statement to Your modifications and<br />
      may provide additional or different license terms and conditions<br />
      for use, reproduction, or distribution of Your modifications, or<br />
      for any such Derivative Works as a whole, provided Your use,<br />
      reproduction, and distribution of the Work otherwise complies with<br />
      the conditions stated in this License.

   5. Submission of Contributions. Unless You explicitly state otherwise,<br />
      any Contribution intentionally submitted for inclusion in the Work<br />
      by You to the Licensor shall be under the terms and conditions of<br />
      this License, without any additional terms or conditions.<br />
      Notwithstanding the above, nothing herein shall supersede or modify<br />
      the terms of any separate license agreement you may have executed<br />
      with Licensor regarding such Contributions.

   6. Trademarks. This License does not grant permission to use the trade<br />
      names, trademarks, service marks, or product names of the Licensor,<br />
      except as required for reasonable and customary use in describing the<br />
      origin of the Work and reproducing the content of the NOTICE file.

   7. Disclaimer of Warranty. Unless required by applicable law or<br />
      agreed to in writing, Licensor provides the Work (and each<br />
      Contributor provides its Contributions) on an "AS IS" BASIS,<br />
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or<br />
      implied, including, without limitation, any warranties or conditions<br />
      of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A<br />
      PARTICULAR PURPOSE. You are solely responsible for determining the<br />
      appropriateness of using or redistributing the Work and assume any<br />
      risks associated with Your exercise of permissions under this License.

   8. Limitation of Liability. In no event and under no legal theory,<br />
      whether in tort (including negligence), contract, or otherwise,<br />
      unless required by applicable law (such as deliberate and grossly<br />
      negligent acts) or agreed to in writing, shall any Contributor be<br />
      liable to You for damages, including any direct, indirect, special,<br />
      incidental, or consequential damages of any character arising as a<br />
      result of this License or out of the use or inability to use the<br />
      Work (including but not limited to damages for loss of goodwill,<br />
      work stoppage, computer failure or malfunction, or any and all<br />
      other commercial damages or losses), even if such Contributor<br />
      has been advised of the possibility of such damages.

   9. Accepting Warranty or Additional Liability. While redistributing<br />
      the Work or Derivative Works thereof, You may choose to offer,<br />
      and charge a fee for, acceptance of support, warranty, indemnity,<br />
      or other liability obligations and/or rights consistent with this<br />
      License. However, in accepting such obligations, You may act only<br />
      on Your own behalf and on Your sole responsibility, not on behalf<br />
      of any other Contributor, and only if You agree to indemnify,<br />
      defend, and hold each Contributor harmless for any liability<br />
      incurred by, or claims asserted against, such Contributor by reason<br />
      of your accepting any such warranty or additional liability.

   END OF TERMS AND CONDITIONS

   APPENDIX: How to apply the Apache License to your work.

      To apply the Apache License to your work, attach the following<br />
      boilerplate notice, with the fields enclosed by brackets "[]"<br />
      replaced with your own identifying information. (Don't include<br />
      the brackets!)  The text should be enclosed in the appropriate<br />
      comment syntax for the file format. We also recommend that a<br />
      file or class name and description of purpose be included on the<br />
      same "printed page" as the copyright notice for easier<br />
      identification within third-party archives.

   Copyright [yyyy] [name of copyright owner]

   Licensed under the Apache License, Version 2.0 (the "License");<br />
   you may not use this file except in compliance with the License.<br />
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software<br />
   distributed under the License is distributed on an "AS IS" BASIS,<br />
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.<br />
   See the License for the specific language governing permissions and<br />
   limitations under the License.


   --- APPLIED [LLVM] EXCEPTIONS TO THE APACHE 2.0 LICENSE ---

   As an exception, if, as a result of your compiling your source code,<br />
   portions of this Software are embedded into an Object form of such source<br />
   code, you may redistribute such embedded portions in such Object form<br />
   without complying with the conditions of Sections 4(a), 4(b) and 4(d) of<br />
   the License.

   In addition, if you combine or link compiled forms of this Software with<br />
   software that is licensed under the GPLv2 ("Combined Software") and if a<br />
   court of competent jurisdiction determines that the patent provision<br />
   (Section 3), the indemnity provision (Section 9) or other Section of the<br />
   License conflicts with the conditions of the GPLv2, you may retroactively<br />
   and prospectively choose to deem waived or otherwise exclude such Section(s)<br />
   of the License, but only in their entirety and only with respect to the<br />
   Combined Software.


THIRD PARTY NOTICES (PATH - DESCRIPTION - NOTICE TEXT):

./src/assets/spinners.mts

The Spinners JavaScript object contained within is strongly based (includes
some transformation and modification) upon the cli-spinners JSON object that is
found at https://github.com/sindresorhus/cli-spinners, under the following
License:

MIT License

Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (https://sindresorhus.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
